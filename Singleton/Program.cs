﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Iron
    {
        public Program ironProgram { get; set; }
        public void Load(string nameProgram)
        {
            ironProgram = Program.getInstance(nameProgram);
        }
    }
    class Program
    {
        private static Program _instance;

        private static object _lock = new object();

        public string nameProgram;
        private Program(string nameProgram)
        {
            this.nameProgram = nameProgram;
        }

        // Статический метод, управляющий доступом к экземпляру одиночки.
        //
        // Эта реализация позволяет вам расширять класс Одиночки, сохраняя
        // повсюду только один экземпляр каждого подкласса.
        public static Program getInstance(string nameProgram)
        {
            lock (_lock)
            {
                // Оператор слияния, выглядит как (_instance != null) ? _instance : new Singleton()
                return _instance ?? (_instance = new Program(nameProgram));
            }
        }
    }
    class Example
    {
        static void Main(string[] args)
        {
            Iron iron = new Iron();
            iron.ironProgram = Program.getInstance("PRO");
            Console.WriteLine(iron.ironProgram.nameProgram);

            Console.ReadKey();
        }
    }
}
